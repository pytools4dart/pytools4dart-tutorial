This repository contains the notebooks for the pytools4dart tutorial. 
It has been updated for DART v1329 and pytools4dart v1.1.20.

The examples and user guides used as notebooks were extracted from pytools4dart repository:
- https://gitlab.com/pytools4dart/pytools4dart/-/tree/master/pytools4dart/examples
- https://gitlab.com/pytools4dart/pytools4dart/-/tree/master/docs/user_guides


# Install pytools4dart

If no conda is installed, install [Miniforge3](https://github.com/conda-forge/miniforge#miniforge3)

For Win users, open Miniforge from Start Menu.

If you already have Anaconda or Miniconda installed, you are entering a grey zone. It seems that Anaconda and Miniconda recently itegrated `mamba` solver (much faster than old `conda` solver), see "merv" comment in [ref](https://stackoverflow.com/questions/76760906/installing-mamba-on-a-machine-with-conda). So you may try to install `pytools4dart` environment with `conda`. If your Anaconda is old, suggestions are to remove Anaconda package and replace it by Miniforge3. But as it may also remove your current Anaconda environments, I would suggest installing Miniforge3 in a separate directory (e.g. the recommended `~/miniforge3`). This will change the initialisation script (usually in `~/.bashrc` on linux, or e.g. profile1.ps on Windows), but it will keep the `anaconda3` directory as is (tested on linux with `conda env list`), you should still be able to activate old anaconda environments, and it lets you come back to the old conda config with `anaconda3/bin/conda init` if needed. Still be careful not to mix package installation with miniforge on anaconda already created environments as it may corrupt them with incompatible dependencies coming from both conda-forge and ananconda default channels.

Install pytools4dart environement and activate it:
```shell
mamba env create -n myptd -f https://gitlab.com/pytools4dart/pytools4dart/-/raw/master/environment.yml
conda activate myptd
```

It can return a warning on the default obj reader, but no error.

Notebooks can be downloaded cloned and __enter the directory__ with the following:
```shell
git clone https://gitlab.com/pytools4dart/pytools4dart-tutorial.git
cd pytools4dart-tutorial
```

This tutorial was tested with DART v1341 (stable version on 2024-06-06), but it should work with other versions.

If not already installed, download DART zip file from https://dart.omp.eu/#/getDart (register for free if needed).
DART can be installed and pytools4dart configured at the same time with the following code, in a python session of `myptd` environment:
```python
from path import Path
import pytools4dart as ptd
dart_zip = r'<path to DART zip>'
dart_dir = Path(r'<path to DART directory>') # e.g. inside pytools4dart-tutorial to keep things cleanable easily
ptd.dart.install(dart_zip, dart_dir)
# and exit the session
exit()
```

If you already have a version of DART installed, just configure `pytools4dart` with that version:
```python
import pytools4dart as ptd
ptd.configure(r'<path to DART directory>') # e.g. r"~/DART", r"C:\DART"
```


# Add R (optional)

Simulations can also be scripted in R, such as in `examples/use_case_0.R`.
In order to do so, R and some R packages must be added to the environment `myptd`. It can be done with the following:
```shell
mamba env update -n myptd -f https://gitlab.com/pytools4dart/pytools4dart-tutorial/-/raw/main/environment-R.yml
```

# Add Jupyter lab (optional)

If you are not used to a particular python IDE (e.g. pycharm, vscode), 
the simplest way to browse the use cases and the user guides while praticing would be through jupyterlab.

In order to have jupyterlab added to your existing `pytools4dart` envrionment:
```shell
mamba env update -n myptd -f https://gitlab.com/pytools4dart/pytools4dart-tutorial/-/raw/main/environment-lab.yml
```

Run Jupyter-lab with:
```shell
jupyter lab --ip 0.0.0.0
```
and `Ctrl+click` on the generated link `http://127.0.0.1:8888/lab?token=xxxx`.

In order to open a python scripts or a markdown files as jupyter notebooks inside jupyter-lab,
right click on the file and choose `Open with ... > Notebook`.


# Troubleshooting

For some use cases run in a jupyter notebook or jupyter-lab, there may be an error raised with message:
```
ModuleNotFoundError: No module named 'matplotlib_inline'
```

This issue was notified in https://gitlab.com/pytools4dart/pytools4dart/-/issues/35. It can be solved installing the missing packages, as presented in the issue comments. The [solution coded in python](https://gitlab.com/pytools4dart/pytools4dart/-/issues/35#note_1436430737) can be executed from inside a jupyter notebook.

